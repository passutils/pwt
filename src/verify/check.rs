use std::{io::Write, thread, time::Duration};

pub fn check_password(times: usize, pass: String) {
    let mut i = usize::default();
    while i != times {
        print!("Enter password: ");
        check_for(
            &Some(pass.clone()),
            || {
                i += 1;
                println!("{}/{} times correct!", i, times);
                thread::sleep(Duration::from_secs(1));
                clearscreen::clear().unwrap();
            },
            || {
                println!("Wrong!");
                print!("Hint? (Y/n) ");
                check_for(
                    &Some(String::from("y")),
                    || {
                        println!("The password is {}", pass);
                        print!("Press enter when you're ready ");
                        check_for(
                            &None,
                            || {
                                clearscreen::clear().unwrap();
                            },
                            || {},
                        );
                    },
                    || {},
                );
            },
        );
    }
}

pub fn check_for(
    text: &Option<String>,
    found: impl FnOnce() -> (),
    not_found: impl FnOnce() -> (),
) -> bool {
    std::io::stdout().flush().unwrap();
    let input = std::io::stdin().lines().next().unwrap().unwrap();
    if text.is_none() || input == text.as_ref().unwrap().to_lowercase() {
        (found)();
        return true;
    }
    (not_found)();

    false
}
