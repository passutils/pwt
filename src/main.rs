use std::env;

use pwt::verify::check::*;

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut times = 5;
    let mut pass = String::from("");
    if args.len() > 1 {
        pass = args[1].clone();
    }
    if args.len() > 2 {
        times = args[2].parse::<usize>().unwrap();
    }
    if pass == "" {
        println!("Usage: pwt <password> [times]");
        return;
    }
    start(times, pass);
    println!("Congratulations!");
}

fn start(times: usize, pass: String) {
    clearscreen::clear().unwrap();

    println!("Take a moment to memorize the following password: {}", pass);
    print!("Press enter when you're ready ");
    check_for(&None, || {}, || {});
    clearscreen::clear().unwrap();
    check_password(times, pass.clone());

    print!("Continue? (Y/n) ");
    check_for(
        &Some(String::from("y")),
        || {
            start(times, pass);
        },
        || {},
    );
}
